# Installation de Xubuntu sur machine virtuelle (VirtualBox)

## Première étape: téléchargement du fichier .ISO
J'ai commencé par télécharger le fichier .ISO de Xubuntu sur  _[leur site officiel](https://xubuntu.org/ "Site officiel de Xubuntu")_ en allant dans la rubrique "_Get Xubuntu -> Download Xubuntu_". Mon navigateur refusant l'accès au "_Mirror download_" Français, j'ai donc pris la version "_LTS 20.04 Focal Fossa_" dans le "_Mirror download_" Allemand car il fallait en prendre un proche de notre localisation.

![Page de téléchargement de Xubuntu](img/CreationMachineVirtuelle/Capture1.png "Page de téléchargement de Xubuntu")

## Deuxième étape: création de la machine virtuelle

Après avoir téléchargé le fichier .ISO, nous allons passer à la création de la machine virtuelle.

![Création de la machine virtuelle](img/CreationMachineVirtuelle/Capture2.png "Création de la machine virtuelle")

Après avoir cliqué sur l'icône suivante, une fenêtre apparaît qui nous demande de choisir le nom de la machine ainsi que le type d'OS (Windows, Mac, Linux, ...) et la version (Ubuntu, Fedora, Windows 7, ...).

J'ai tout simplement mis le nom "_Xubuntu_" pour la machine. J'ai choisi le type __Linux__ ainsi que la version __Ubuntu 64-bit__ pour l'OS, puisque Xubuntu est un dérivé plus léger de Ubuntu :

![Création de la machine virtuelle](img/CreationMachineVirtuelle/Capture3.png "Création de la machine virtuelle")

### 1. Choix pour la mémoire

La documentation officielle (rubrique "_Get Xubuntu -> System Requirements_", catégorie "_Minimum system requirements_") recommande au moins __512 Mo__ de mémoire :

![Recommandations pour la taille de la mémoire](img/CreationMachineVirtuelle/Capture4.png "Recommandations pour la taille de la mémoire")

J'ai donc laissé le paramètre par défaut "__1024 Mo__", qui est largement suffisant et qui est recommandé par VirtualBox comme cette capture le montre :

![Choix de la taille de la mémoire](img/CreationMachineVirtuelle/Capture5.png "Choix de la taille de la mémoire")

### 2. Création du disque dur virtuel

La page suivante me laisse le choix entre ne pas créer de disque dur virtuel, en créer un ou en utiliser un déjà existant.

J'ai laissé la valeur par défaut "_Créer un disque dur virtuel maintenant_" pour des raisons de simplicité, puisque je n'ai pas d'autres disques dur virtuels à ma dispostion et que cela n'a pas vraiment d'intérêt de lancer une machine virtuelle sans :

![Choix de la création du disque dur virtuel](img/CreationMachineVirtuelle/Capture6.png "Choix de la création du disque dur virtuel")

Ici j'ai laissé la case "_VDI (Virtual Disk Image)_" cochée par défaut, pour des raisons de simplicité mais aussi parce que je ne connais pas les autres options.

![Choix de la création du disque dur virtuel](img/CreationMachineVirtuelle/Capture7.png "Choix de la création du disque dur virtuel")

J'ai aussi laissé la case "_Dynamiquement alloué_" par défaut pour des raisons de simplicité, puisque le disque dur virtuel avec taille dynamiquement allouée est plus rapide à créer que celui à taille fixe. Le premier type utilisera l'espace sur mon disque physique qu'au fur et à mesure qu'il se remplira.

![Choix de la création du disque dur virtuel](img/CreationMachineVirtuelle/Capture8.png "Choix de la création du disque dur virtuel")

### 3. Choix de la taille du stockage

Quant au stockage, la documentation officielle (rubrique "_Get Xubuntu -> System Requirements_", catégorie "_Recommended system resources_") recommande au moins __20 Go d'espace libre__.

Ce qui permet d'installer de nouvelles applications et de sauvegarder nos données personnelles sur le disque dur virtuel en plus de l'OS :

![Recommandations pour la taille du stockage](img/CreationMachineVirtuelle/Capture9.png "Recommandations pour la taille du stockage")

Cependant, VirtualBox utilise les Gio (Gibioctets). J'ai donc utilisé un convertisseur en ligne pour ne pas perdre plus de temps et connaître l'équivalent de 20 Go en Gio:

>_20 Go = environ 18,6264 Gio_
>
>_20 x 0,93132257461548 = 18,6264514923096 = environ 18,6264 Gio_

J'ai donc décidé de prendre 20 Gio au cas-où :

![Choix de la taille du stockage](img/CreationMachineVirtuelle/Capture10.png "Choix de la taille du stockage")

__________

Maintenant que notre machine virtuelle est créée, nous allons donc passer à l'installation complète de Xubuntu.

![Texte alternatif](img/CreationMachineVirtuelle/Capture11.png "Création de la machine virtuelle finalisée")

## Troisième étape: installation complète de Xubuntu en Français

Lors du lancement de notre machine virtuelle, VirtualBox nous demande de choisir le disque de démarrage. C'est ici que nous devons donner le fichier .ISO de Xubuntu que nous avons téléchargé à la première étape pour commencer l'installation. 

Cela peut paraître évident, mais la machine ne fonctionnera pas si l'on n'en choisi pas un :

![Choix disque de démarrage](img/InstallationComplete/Capture1.png "Choix disque de démarrage")

J'avais hésité au début à choisir l'installation complète en Anglais, mais j'ai préféré la faire en Français pour des raisons de simplicité et ainsi éviter de commettre des erreurs dans l'installation suite à une mauvaise compréhension.

![Début de l'installation de Xubuntu](img/InstallationComplete/Capture2.png "Début de l'installation de Xubuntu")

Xubuntu me demande ensuite de choisir la disposition du clavier. Je l'ai mise en Français AZERTY puisque c'est celle qui convient le mieux à mon ordinateur :

![Disposition du clavier](img/InstallationComplete/Capture3.png "Disposition du clavier")

Dans la fenêtre suivante, Xubuntu me laisse le choix entre les cases "_Télécharger les mises à jour pendant l'installation de Xubuntu_" et "_Installer un logiciel tier pour le matériel graphique et Wi-Fi et des formats supplémentaires_". 

J'ai laissé la première case cochée par défaut car elle permet d'avoir la dernière version sans problème après l'installation. Je n'ai pas coché la seconde car je n'y voyais pas l'intérêt de le faire.

![Choix des mises à jour](img/InstallationComplete/Capture4.png "Choix des mises à jour")

Dans la fenêtre suivante, Xubuntu me demande de choisir le type d'installation. Il me dit qu'il n'a détecté aucun autre OS sur le disque dur virtuel.

J'ai laissé la case "Effacer le disque et installer Xubuntu" cochée par défaut puisque nous voulons une installation complète de l'OS :

![Choix du type d'installation](img/InstallationComplete/Capture5.png "Choix du type d'installation")

Comme dans n'importe quel OS (Windows, Mac, autres Linux, ...), Xubuntu m'a ensuite demandé de donner un nom et un mot de passe pour créer ma session.

J'ai coché la case "_Demander mon mot de passe pour ouvrir une session_" par mesure de sécurité. Cette option me demande le mot de passe de ma session à chaque fois que je lance ma machine virtuelle.

L'autre case "_Ouvrir la session automatiquement_" ouvre automatiquement ma session sans demander mon mot de passe, __uniquement au moment du démarrage de la machine virtuelle__. Lorsque je la fermerais (sans éteindre la machine), Xubuntu me redemandera le mot de passe pour la déverrouiller.

![Création de ma session](img/InstallationComplete/Capture6.png "Création de ma session")

Après avoir renseigné le formulaire et cliqué sur "_Continuer_", l'installation est finalement lancée. Il ne reste plus qu'à attendre pendant quelques minutes le temps qu'elle se finisse.

![Installation de Xubuntu lancée](img/InstallationComplete/Capture7.png "Installation de Xubuntu lancée")

Après cela, Xubuntu me demande ensuite de redémarrer la machine virtuelle pour finaliser correctement l'installation et lui appliquer les nouveaux paramètres. Ceci fait, nous aurons finalement un accès complet à notre tout nouvel OS !

![Demande de redémarrage de la machine](img/InstallationComplete/Capture8.png "Demande de redémarrage de la machine")
